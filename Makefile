SRC:= $(wildcard *.js)

OBJ=$(addprefix .build/,$(SRC))

.build/%.js: %.js lib/*.js
	awk '{ if (match($$0,/^require\("(.*)"\);$$/,a)) { system("cat lib/"a[1]) } else print }' $< > $@

all : $(OBJ)
