#!/bin/bash

make

if [ "x$1" == "x" ]; then
  bmls='bmls'
else
  bmls=$1
fi

cats=`grep -h @cat *.js | awk -F '=' '{print $2; }' | sort | uniq`

echo "<html><style>div.bm_hiddentext { display:none;position:absolute;left:12em;background-color:white;padding:5px;border:solid 1px;}div.bookmarklet:hover div.bm_hiddentext{display:block;}</style>" > bookmarklets.txt

mkdir -p .build

for cat in $cats; do

	catjs=`grep -l "@cat=$cat" *.js`
	echo "<h3>$cat</h3>"
	for js in $catjs; do

		desc=`grep @desc $js | awk -F '=' '{print $2; }'`
		title=`grep @title $js | awk -F '=' '{print $2; }'`
		(>&2 echo "$js $title")
		text=$(grep -oPz  '(?s)@text=\K.*(?=@@)' $js | sed ':a;N;$!ba;s#\n#<br />\n#g;s#\x0##g;')
		echo -n "<div class='bookmarklet'><span class='bm_link' style='width:10em;display:inline-block;'><a href=\"javascript:"
		php -r 'echo trim(strtr(shell_exec("uglifyjs '.build/$js' -e -m"),array("\""=>"%22"," "=>"%20")));'
		echo "\">$title</a></span><div class="bm_hiddentext">$text</div><span class='desc'>$desc</span><br/></div>"

	done

done >> bookmarklets.txt

echo "</html>" >> bookmarklets.txt

echo -n "" > extmenu.html

for cat in $cats; do

        catjs=`grep -l @cat=$cat *.js`
	set -- $catjs
	scat=`grep @scat $1 | awk -F '=' '{print $2; }'`
        echo "<div class='$scat'><strong>$cat</strong></div>"
        for js in $catjs; do

		scat=`grep @scat $js | awk -F '=' '{print $2; }'`
                desc=`grep @desc $js | awk -F '=' '{print $2; }'`
                title=`grep @title $js | awk -F '=' '{print $2; }'`
		class=`grep @class $js | awk -F '=' '{print $2; }'`
                (>&2 echo "$js $title")
                text=$(grep -oPz  '(?s)@text=\K.*(?=@@)' $js | sed ':a;N;$!ba;s#\n#<br />\n#g;s#\x0##g;')
                echo -n "<div id='${js/.js/}' class='button bookmarklet ${scat} ${class}'><span id='${js/.js/}' class='bm_link' style='width:10em;display:inline-block;' title='$desc'>"
                echo -n "$title</span>"
		# <div class="bm_hiddentext">$text</div>"
		# echo -n "<span class='desc'>$desc</span>"
		echo "</div>"
        done

done >> extmenu.html

chmod a+r *.*

rsync -va .build/*.js agw:/data/alma/html/${bmls}/
scp extmenu.html agw:/data/alma/html/${bmls}/menu.html
scp bookmarklets.txt agw:/data/alma/html/bookmarklets.html
