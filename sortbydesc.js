/*
 * @scat=other
 * @cat=Sonstiges
 * @desc=Exemplare sortieren nach Beschreibung/Description
 * @title=SortByDesc
 * @text=sortiert nach Beschreibung/Description
  und springt dann zum ersten item mit Eingangsdatum@@
 */
$("#pageBeansortSectionPropertiesroutine_hiddenSelect option[value='Description']").prop('selected',true).trigger('change');
setTimeout(function(){
  $('#widgetId_Left_sortroutine').removeClass('open');
  $('#TABLE_DATA_list').find("span[id$='_COL_arrivalDate'][title!='-']").get(0).scrollIntoView(false);
  window.scrollBy(0,200);
},1000);
