/*
 * @scat=se
 * @class=nmde mde
 * @cat=Sacherschließung
 * @desc=MDEditor: ersten 983er in 852 8 $$h der holding kopieren
 * @title=add $$h+SYS+EJ
 * @text=kopiert die erste TUW-Systemantik (983) als Aufstellungssystematik
 * und das Erscheinungsjahr aus 008
 * öffnet zuvor wenn notwendig die richtige Holding (UNASSIGNED)
 * und speichert@@
 *
 */
var ST = {
  Start: 1,
  OpenHolding: 2,
  EditHolding: 3,
  SaveHolding: 4,
  AllDone: 255,
}

var state = ST.Start;

require("bmls_lib.js");

require("nmde_lib.js");

require("mdeh_lib.js");

function create_sig(){
  edTs = mde.find("table[id^='editorTable']");
  var f852_8 = findField(edTs,'852',{}).find("textarea");
  if (f852_8.length == 0) return false;
  var f852_8_val = f852_8.val();
  var re = /^(.*)(\$\$c\s+)(.*?)(\$\$.*|$)/;
  var match = re.exec(f852_8_val);
  console.log(match);
  if (!!match && match[2].trim() != ''){
    if (!match[2] || match[3].trim() == 'UNASSIGNED' || match[3].trim() == '') match[3] = 'UNASSIGNED';
    if (!match[4]) match[4] = '';

    f852_8_val = match[1].trim() + ' ' + match[2].trim() + ' ' + match[3].trim() + ' ' + match[4];
    console.log(f852_8_val);
  } else {
    f852_8_val = f852_8_val.trim() + ' $$c UNASSIGNED';
  }
  console.log(match);
  re = /(.*)\s*\$\$h\s*(.*?)( \$\$.*)?$/;
  match = re.exec(f852_8_val);
  console.log(match);
  if (!match || match[2] == '' || match[2].substr(0,2) == '$$' || confirm('Bestehende Signatur '+match[2]+' ersetzen?')){
    console.log('$$h empty');
    if (!match)
      f852_8.val(f852_8_val.trim());
    else if (match[2] == '')
      f852_8.val(match[1].trim());
    else if (match[2].substr(0,2) == '$$')
      f852_8.val(match[1].trim() + ' ' + match[2].trim());
    else
      f852_8.val(match[1].trim()+' $$d '+match[2].trim());
    if (!!match && match[3] != undefined)
      f852_8.val(f852_8.val()+match[3]+' $$h');
    else
      f852_8.val(f852_8.val()+' $$h');
    f983 = findField(edTs,'983',{}).find("textarea");

    var resys = /([A-Z]{3}\:\d{3})( [A-ZÄÖU\-][A-ZÄÖÜß0-9\-]{2})?(\:.*?)?($| \$\$)/;
    var tusys = resys.exec(f983.val());
    console.log(tusys);
    if (tusys){
      var f08=findField(edTs,'008',{}).find("textarea");
      console.log(f08.val());
      var yre=/([12][09]\d{2})/;
      var yearm=yre.exec(f08.val().substr(7,4));

      var date='';
      if (yearm && yearm[1] != '')
        date = yearm[1];
      else {
        msg += 'Kein Jahr gefunden! ';
        col = '#ff8f8f';
      }
      if (tusys[1].substr(0,3) == 'ALL'){
        f852_8.focus().click().val(f852_8.val().trim() + ' ' + tusys[1] + tusys[2] + tusys[3]).get(0).dispatchEvent(change);
        msg += "Signatur: " + tusys[1] + tusys[2] + tusys[3];
        state = ST.SaveHolding;
        save();
      } else if (tusys[2] == undefined){
        f852_8.focus().click().val(f852_8.val().trim() + ' ' + tusys[1] + ' (' + date + ')').get(0).dispatchEvent(change);
        msg += "Signatur: " + tusys[1] + ' (' + date + ')';
        state = ST.SaveHolding;
        save();
        // mde.get(0).dispatchEvent(cas);
      } else {
        $.post("https://almagw.ub.tuwien.ac.at/tulama/astsys.php",{QUERY: tusys[1] + tusys[2]}, function(data){
          if (data != undefined && data == '1'){
            f852_8.focus().click().val(f852_8.val().trim() + ' ' + tusys[1] + tusys[2] + ' (' + date + ')').get(0).dispatchEvent(change);
            msg += "Signatur: " + tusys[1] + tusys[2] + ' (' + date + ')';
          } else if (data != undefined && data == '0'){
            f852_8.focus().click().val(f852_8.val().trim() + ' ' + tusys[1] + ' (' + date + ')').get(0).dispatchEvent(change);
            msg += "Signatur: " + tusys[1] + ' (' + date + ')';
          } else {
            f852_8.focus().click().val(f852_8.val().trim() + ' ' + tusys[1] + tusys[2] + ' (' + date + ')').get(0).dispatchEvent(change);
            msg += "Aufstellungssystematik mit Zusatzcode konnte nicht automatisch überprüft werden " + tusys[1] + tusys[2] + " (" + data +")!";
            col = '#ff8f8f';
          }
          state = ST.SaveHolding;
          setTimeout(save,500);
          // mde.get(0).dispatchEvent(cas);
        }).fail(function() {
          f852_8.focus().click().val(f852_8.val().trim() + ' ' + tusys[1] + tusys[2] + ' (' + date + ')').get(0).dispatchEvent(change);
          msg += "Aufstellungssystematik mit Zusatzcode konnte nicht automatisch überprüft werden!" + tusys[1] + tusys[2] + ' (' + date + ')';
          col = '#ff8f8f';
          state = ST.SaveHolding;
          save();
          // mde.get(0).dispatchEvent(cas);
        });
      }
    } else {
      msg += 'Keine (gültige) Systematik im ersten 983?';
      col = '#ff8f8f';
      allDone();
    }
  } else {
    msg += '$$h existiert schon';
    col = '#8fffff';
    allDone();
  }
  // f852_8.click().focus().get(0).dispatchEvent(change);
  // state = ST.SaveHolding;
  // mdeditor.get(0).dispatchEvent(save);
}

function allDone(){
  state = ST.AllDone;
  toast(msg,col);
  console.log('main observer disconnect');
  clearTimeout(obstimer);
  observer.disconnect();
  lAma_unblock();
}

var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    // console.log(mutation);
    if (lBremoved(mutation)){
      console.log(state);
      switch(state) {
        case ST.OpenHolding:
          edit_holding('UNASSIGNED');
          break;
        case ST.EditHolding:
          setTimeout(create_sig,500);
          break;
        case ST.SaveHolding:
          allDone();
          break;
      }
    }
  });
});
var edTs;
initMDE(observer).then(function(nmde){
  obstimer = setTimeout(function(){
    console.log('main observer timed out');
    msg += 'Main observer timed out! ';
    col = '#ff0808';
    allDone();
  },120000);
  edTs = nmde.mde.find("table[id^='editorTable']");
  var f852_8 = findField(edTs,'852',{});
  if (f852_8.length == 1){
    create_sig();
    allDone();
  } else {
    open_holding();
  }
}).catch((e)=>{
  console.log(e);
  msg += e;
  col = '#ff8f8f';
  allDone();
});
