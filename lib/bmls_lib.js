var msg = '';
var col = '#8fff8f';

function toast(msg, col, duration = 9){
  // message am unteren rand ~10 sec lang, col als hintergrund
  // einmal drauf clicken: nachricht beliebt stehen
  // ein zweitesmal drauf clicken: nachricht wird entfernt.
  var toast = $("<div style='bottom: 0; display: none; background: " + col + "; padding: 5px; flex-grow: 1;'>" + msg + "</div>");
  $('#tu_lAma_toast').append(toast);
  toast.fadeIn(1000).delay(duration*1000).fadeOut(1000);
  var del = setTimeout(function(){
    toast.remove();
  }, duration*1000+3000);
  toast.click(function(){
    clearTimeout(del);
    toast.stop(true).show().click(function(){toast.remove();});
  });
}
