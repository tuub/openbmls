
  function open_holding (){
    if (mde.find("[id^='MarcEditorPresenter.textArea.852'] textarea").length > 0){
      change_subloc();
    } else if (mde.find('#SearchExternalResults').length > 0){
      edit_holding ('9190 - Hochschulschriften');
    } else {
      console.log(state + ' open holding');
      state = ST.OpenHolding;
      mde.get(0).dispatchEvent(ci);
    }
  }
  
  function edit_holding (standort){
    console.log(state + ' open for editing');
      state = ST.EditHolding;
      var edit = mde.find('#RecordResultsPresenter\\.resultTitlePanel')
        .filter(function(n,e){
          return $(e).text().includes(standort);
        }).closest('table.resultContainerStyle')
        .find('#RecordResultsPresenter\\.resultActionPanel div:contains(Bearbeiten),#RecordResultsPresenter\\.resultActionPanel button:contains(Bearbeiten),#RecordResultsPresenter\\.resultActionPanel div:contains(Edit),#RecordResultsPresenter\\.resultActionPanel button:contains(Edit)');
      if (edit.length >= 1){
        edit.get(0).dispatchEvent(click);
      } else {
        console.log(state + ' nothing found');
        msg += ' Kein entsprechender Bestand vorhanden!';
        allDone();
      }
  }
                                        
  function open_accession_number(){
    console.log('open accession number');
    state = ST.OpenAccession;
    mde.get(0).dispatchEvent(sca);
  }
  
  function add_accession_number(label){
    state = ST.AddedAccession;
    console.log(state + ' add accession number');
    var dbox = mde.find(".gwt-DialogBox");
    // zeile rausfiltern in der nur das label steht, ids sind nicht fix (gwt...)
    var hb =  dbox.find("div.gwt-Label:contains("+label+")").filter(function () {
      return $(this).text() == label;
    }).closest('tr');
    // auch die buttons haben keinen namen value oder id, aber vor der auswahl des radio buttons ist er disabled
    var ok = dbox.find("button:disabled");
    // hb anklicken
    if (hb.length >= 1){
      hb.find("td span.gwt-RadioButton input").click().get(0).dispatchEvent(click);
      // OK/Erzeugen anklicken
      ok.click().get(0).dispatchEvent(click);
    }
  }
