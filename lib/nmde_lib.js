// new mde
var nMdeW;
var mde;
var ads;

var click = document.createEvent('HTMLEvents');
click.initEvent('click', true, true);

var down = document.createEvent('HTMLEvents');
down.initEvent('mousedown', true, true);

var change = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, charCode :0, keyCode: 37});
var change1 = document.createEvent('HTMLEvents');
change1.initEvent('keyup', false, true);

// keyboard shortcuts: mde.get(0).dispatchEvent(f8)
// neues Feld
var f8 = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, charCode :0, keyCode: 119});
// Entwurf speichern
var cas = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, key : "s", charCode :0, keyCode: 83, ctrlKey : true, altKey: true});
// Speichern
var cs = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, key : "s", charCode :0, keyCode: 83, ctrlKey : true, altKey: false});
// Feld(er) löschen
var cf6 = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, charCode :0, keyCode: 117, ctrlKey : true});
// new local field ctrl-l
var cl = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, key : "l", charCode :0, keyCode: 76, ctrlKey : true});
// open holding ctrl-i
var ci = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, key : "i", charCode :0, keyCode: 73, ctrlKey : true});
// Zugangsnummer erzeugen
var sca = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, key : "a", charCode :0, keyCode: 65, ctrlKey : true, shiftKey: true});

function saveDraft(){
  mde.get(0).dispatchEvent(cas);
}
function save(){
  mde.get(0).dispatchEvent(cs);
}

// usage: findField(ads,'245',{}).val(newvalue).focus().click().get(0).dispatchEvent(change)
function findField (domo, tag, { ind1=null, ind2=null, val=null }){
  return domo.find("tr:has([id^='MarcEditorPresenter.textArea."+tag+"'])").filter(function(){
    return (!ind1 || $(this).find("[id^='FieldIndicatorBox.tag."+tag+"'][id$='.1']").val() == ind1)
        && (!ind2 || $(this).find("[id^='FieldIndicatorBox.tag."+tag+"'][id$='.2']").val() == ind2)
        && (!val || $(this).find('textarea').val().match(val))
  });
}

var tags;
var lastTags;
var lastTagsSeen;
var afobstimer;

var addfieldobserver = new MutationObserver(function(mutations) {    
  mutations.forEach(function(mutation) {
    // ein neuse leeres feld, tag und value ausfüllen
    // console.log(mutation);
    // console.log($(mutation.addedNodes).find("input[id^='FieldTagBox.tag']").val());
    if (state == FieldAdd && mutation.addedNodes.length > 0
        && $(mutation.addedNodes).find("input[id^='FieldTagBox.tag']").length > 0
        && $(mutation.addedNodes).find("input[id^='FieldTagBox.tag']").val() == lastTags[0]){
      lastTagsSeen += 1;
      if (lastTagsSeen == lastTags.length){
        lastTagsSeen = 0;
        clearTimeout(afobstimer);
        addfieldobserver.disconnect();
        setTag();
      }
    }
  });
});

var newTag;
var newInd1;
var newInd2;
var newVal;
var FieldAdd = 2;

function addField(tag, val, {
      ind1 = null,
      ind2 = null,
      insTag = null,
      insInd1 = null,
      insInd2 = null,
      reuse = false,
      sval = null,
      local = false,
      newState = null
    }){

  var target;

  if (state != ST.AllDone){
    newVal = val;
    newInd1 = ind1;
    newInd2 = ind2;
    newTag = tag;
    if (!!newState){
      state = newState;
      FieldAdd = newState;
    }
    if (!!reuse && findField(ads, tag, {ind1:ind1, ind2:ind2, val:sval}).length > 0){
      state += 1;
      setIndVal(reuse);
    } else {
      lastTagsSeen = 0;
      addfieldobserver.observe(ads.get(0),{childList:true,subtree:true});
      afobstimer = setTimeout(function(){
        console.log('add field observer timed out');
        addfieldobserver.disconnect();
        allDone();
      },10000);
      if (!!insTag) target = findField(ads, insTag, {ind1: insInd1, ind2: insInd2});
      else target = findField(ads, tags.filter(function(i,x){ return x <= newTag}).get(-1), {});
      if (target.find('[id^="FieldTagBox.tag."]').val() <= '009' && newTag > '009'){
        target = findField(ads, tags.filter(function(i,x){ return x >= newTag && x != 'LDR'}).get(0), {});
      }
      target.find('textarea').eq(-1).click().focus();
      if (local) mde.get(0).dispatchEvent(cl);
      else mde.get(0).dispatchEvent(f8);
    }
  }
}

function setTag(){
  console.log(state + ': ' + newTag);
  var newField = ads.find("[id^='MarcEditorPresenter.textArea..'] textarea");
  if (newField.length == 1) {
    state += 1;
    ads.find("[id='FieldTagBox.tag.'],[id^='FieldTagBox.tag..']").click().focus().val(newTag).get(0).dispatchEvent(change1);
    // mde.get(0).dispatchEvent(cas);
  }
}

function setIndVal(reuse=false){
  var newField
  if (!!reuse)
    newField = findField(ads, newTag, {ind1: newInd1,ind2: newInd2});
  else
    newField = findField(ads, newTag, { val: /^(\$\$a)? *$/ });
  
  if (!!newInd1 && newField.find("[id^='FieldIndicatorBox.tag."+newTag+"'][id$='.1']").val() != newInd1){
    console.log(state + ' '+ newTag +' ind1 ='+newInd1);
    newField.find("[id^='FieldIndicatorBox.tag."+newTag+"'][id$='.1']").focus().click().val(newInd1).get(0).dispatchEvent(change1);
    mde.get(0).dispatchEvent(cas);
  } else if (!!newInd2 && newField.find("[id^='FieldIndicatorBox.tag."+newTag+"'][id$='.2']").val() != newInd2){
    console.log(state + ' '+ newTag +' ind2 ='+newInd2);
    newField.find("[id^='FieldIndicatorBox.tag."+newTag+"'][id$='.2']").focus().click().val(newInd2).get(0).dispatchEvent(change1);
    mde.get(0).dispatchEvent(cas);
  } else if (newField.find('textarea').val() != newVal){
    state += 1;
    newField.find('textarea').click().focus().val(newVal).get(0).dispatchEvent(change);
    mde.get(0).dispatchEvent(cas);
  }
}

function lAma_unblock(){
  lAma_blocker.remove();
  nmdeW_lAma_blocker.remove();
  mde_lAma_blocker.remove();
}

function lBremoved(mutation){
  return (mutation.removedNodes.length >0 && $(mutation.removedNodes).filter('div.gwtVisibility').length > 0 );
}

var lAma_blocker = $('<span id="tu_lAma_blocker"></span>');
var nmdeW_lAma_blocker = $('<span id="tu_lAma_blocker"></span>');
var mde_lAma_blocker = $('<span id="tu_lAma_blocker"></span>');

function initMDE(observer = null){
  return new Promise(function(resolv, reject){
    nMdeW = $('iframe#yardsNgWrapper');
    if (nMdeW.length == 0){
      reject('geht nur im neuen MDE');
    } else if ($('#tu_lAma_blocker').length > 0){
      reject('Jeder nur ein Kreuz bitte!');
    } else {
      nMdeW = nMdeW.contents();
      mde = nMdeW.find('iframe#yards_iframe').contents();
      ads = mde.find(".showBorderExlibris table[id^='editorTable']");
      if (ads.length == 0)
          ads = mde.find("table[id^='editorTable']");
      if (ads.length == 0){
        reject('Kein aktiver Titel-DS gefunden');
      } else {
        // alle tags
        tags = ads.find('input[id^="FieldTagBox\\.tag"]').map(function(i,n){return n.value;});
        // die letzten gleichen tags
        lastTags = tags.filter(function(i,tag){ return tag == tags.last().get(0); });

        $('body').append(lAma_blocker);
        nMdeW.find('body').append(nmdeW_lAma_blocker);
        mde.find('body').append(mde_lAma_blocker);

        if (!!observer)
          observer.observe(nMdeW.find('mdng-layout').get(0), { childList:true, subtree:false });

        resolv({wrapper: nMdeW, mde: mde, ads, ads});
      }
    }
  });
}
