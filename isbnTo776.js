/*
 * @scat=katalog
 * @cat=Katalogisierung
 * @class=nmde
 * @desc=MDEditor: ISBNs aus 020 nach 776_08 verschieben, ISBN aus doi
 * @title=isbn nach 776
 * @text=alle selektierten ISBNs aus 020 werden in einen 776 verschoben
  dubletten werden entfernt
  eine ISBN aus der doi ()024_7) wird gegebenenfalls übernommen
  und 776_08_n anhand von 008[23]=='o' erzeugt.
  dois bei Print ebenfalls verschoben@@
 *
 */
var ST = {
  Start:        1,
  DelSelected:  2,
  FieldAdd:     3,
  TagSet:       4,
  ValSet:       5,
  AllDone:    255
};

var state = ST.Start;

require("bmls_lib.js");

require("nmde_lib.js");

function delSelected(){
  console.log(state + ': delete selected');
  state = ST.DelSelected;
  mde.get(0).dispatchEvent(cf6);
}

var obstimer;

function allDone(){
  state = ST.AllDone;
  clearTimeout(obstimer);
  observer.disconnect();
  lAma_unblock();
  toast(msg,col);
}

function getIsbns(tags,subf){
  if (subf==undefined) subf='z';
  var ISBNs = new Array();
  tags.each(function(i,e){
    e.value.split('$$').forEach(function(f){
      if (f.length > 0 && f[0] == subf && (match = f.match(/([0-9X\-]{10,17})/i))){
        var isbn = match[1].replace(/-/g,'');
        if (isbn.length == 10 || isbn.length == 13)
          ISBNs.push(isbn)
      }
    });
  });
  return ISBNs;
}

var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    // console.log(mutation);
    if (lBremoved(mutation)){
      switch(state){
        case ST.DelSelected:
          add776();
          break;
        case ST.TagSet:
          setIndVal();
          break;
        case ST.ValSet:
          if (eBook || delDois())
            addDoiIsbn();
          break;
        default:
          allDone();
      }
    }
  });
});

var addDoiIsbn;
var add776;
var delDois;
var eBook;


initMDE(observer).then(function(nmde){
  obstimer = setTimeout(function(){
    console.log('main observer timeout');
    msg += ' main observer timeout';
    col = '#ff8f8f';
    allDone();
  },120000);

  eBook = (findField(nmde.ads,'008',{}).find("textarea").val().substr(23,1) == 'o');
  var f024_7 = findField(nmde.ads,'024',{ind1:'7',val: /\$\$2 (doi|urn|hdl)/}).find("textarea");
  var doi_isbn = false;

  if (f024_7.length > 0 && (match = f024_7.val().match(/\$\$a\s+.*\/(97[89][0-9X\-]{10,14}).*\$\$2\s+doi/i))){
    doi_isbn = match[1].replace(/-/g,'');
  }
  var f020 = findField(nmde.ads,'020',{}).find("textarea");
  var isbns_020 = getIsbns(f020,'a');
  var toMove = nmde.ads.find("tr.selectedRow [id^='MarcEditorPresenter.textArea.020'] textarea");
  var isbns_tMv = getIsbns(toMove,'a').concat(getIsbns(toMove,'z'));
  var f776 = findField(nmde.ads,'776',{}).find("textarea");
  var isbns_776 = getIsbns(f776);
  var f775 = findField(nmde.ads,'775',{}).find("textarea");
  var isbns_775 = getIsbns(f775);
  var keep = f020.not(toMove);
  var isbns_keep = getIsbns(keep,'a');
  var selRows = nmde.ads.find('tr.selectedRow');

  // kein eBook? doi löschen
  var dois = '';
  f024_7.each(function(i,id){
    if (doi = id.value.match(/\$\$a(.*?)(\$\$2 (doi|urn|hdl)|$)/))
      dois += ' $$o '+ doi[1].trim();
  });
  delDois = function(){
    let f024_7 = findField(nmde.ads,'024',{ind1:'7',val: /\$\$2 (doi|urn|hdl)/}).find("textarea");
    console.log(f024_7);
    if (f024_7.length == 0){
      return true;
    } else {
      let doi = f024_7.val().match(/\$\$2 (doi|urn|hdl)/)
      msg += doi[1]+' in print verschoben; ';
      col = '#ffff8f';
      f024_7.val('$$a ').click().focus().get(0).dispatchEvent(change);
      f024_7.blur();
      mde.get(0).dispatchEvent(cas);
      return false;
    }
  }
  // abgleich
  // doi
  if (doi_isbn){
    if (isbns_020.includes(doi_isbn)){
      doi_isbn = false;
    } else if (isbns_775.includes(doi_isbn)){
      doi_isbn = false;
      msg += 'doi in 775 ';
      col = '#ffff8f';
    } else if (isbns_776.includes(doi_isbn)){
      doi_isbn = false;
      msg += 'doi in 776 ';
      col = '#ffff8f';
    }
  }
  if (!eBook && !!doi_isbn){
    isbns_tMv.push(doi_isbn);
  }
  // abgleich 775-776
  isbns_775.forEach(function(isbn){
    if (isbns_776.includes(isbn)){
      msg += isbn + ' in 775 und 776! ';
      col = '#ffff8f';
    }
  });
  // abgleich toMove mit 775
  isbns_tMv = isbns_tMv.filter(function(isbn){return !isbns_775.includes(isbn);});
  // abgleich toMove mit 776
  isbns_tMv = isbns_tMv.filter(function(isbn){return !isbns_776.includes(isbn);});
  // abgleich toMove mit verbleibenden 020
  isbns_tMv = isbns_tMv.filter(function(isbn){return !isbns_keep.includes(isbn);});

  console.log(isbns_020);
  console.log(isbns_keep);
  console.log(isbns_tMv);
  console.log(isbns_776);
  console.log(isbns_775);
  console.log(toMove);
  console.log(selRows);

  addDoiIsbn = function(){
    if (doi_isbn){
      msg += ', isbn aus doi';
      addField('020','$$a '+doi_isbn,{insTag:'009',newState: ST.FieldAdd});
      doi_isbn = false;
    } else {
      allDone();
    }
  };

  if ((!doi_isbn || toMove.length > 0) && toMove.length < selRows.length && (eBook || f024_7.length == 0)){
    msg += 'bitte nur 020 zum Verschieben selektieren!';
    col = '#ff8f8f';
    allDone();
  } else if (isbns_keep.length>0 && (isbns_tMv.length > 0 || isbns_776.length > 0)
      || isbns_keep.length==0 && ((doi_isbn && eBook || !eBook && f024_7.length > 0)  || confirm("wirklich alle ISBNs von 020 zu 776 verschieben?"))){

    var isbns = isbns_776.concat(isbns_tMv);
    add776 = function(){
      if (isbns.length > 0){
        let val776 = '$$i Erscheint auch als $$n '+(eBook?'Druck-Ausgabe':'Online-Ausgabe')+' $$z '+isbns.join(' $$z ')+(eBook?'':dois);
        addField('776',val776,{ind1: '0',ind2: '8', reuse: true, newState: ST.FieldAdd});
      } else if (f024_7.length > 0 && !eBook){
        let val776 = '$$i Elektronische Reproduktion'+dois;
        addField('776',val776,{ind1: '0',ind2: '8', reuse: true, newState: ST.FieldAdd});
      } else
        addDoiIsbn();
    };
        // delete selected 020
    if (toMove.length > 0 && toMove.length == selRows.length){
      msg += toMove.length + ' 020 isbns durch ' + isbns.length + ' in 776 ersetzt.';
      delSelected();
      setTimeout(add776,2000);
    } else if (f776.length < 1){
      msg += isbns.length + ' in 776.';
      add776();
    }
  } else {
    msg += 'na gut dann nicht';
    allDone();
  }
}).catch(function(e){
  console.log(e);
  msg += e;
  col = '#ff8f8f';
  allDone();
})
